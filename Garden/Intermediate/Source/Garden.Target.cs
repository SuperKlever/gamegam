using UnrealBuildTool;

public class GardenTarget : TargetRules
{
	public GardenTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("Garden");
	}
}
